# Extend array class with a bubblesort method
class Array
    # Check whether this array is sorted using the built-in sort method
    def sorted?
        return self == self.sort
    end
    
    # Swap two elements
    def swap(a, b)
        self[a], self[b] = self[b], self[a]
    end
    
    # Implementation of bubble sort
    def bubblesort!
        sorted = false
        while !sorted do
            number_of_switches = 0
            for i in 0..(self.length - 2)
                # if elements are in the wrong order, swap
                if self[i] > self[i+1]
                    self.swap(i, i+1)
                    # record number of switches
                    number_of_switches += 1
                end
            end
            
            if number_of_switches == 0
                sorted = true
            end
        end
        
        return self
    end
end

# Test cases
puts [].bubblesort!.sorted?
puts [1].bubblesort!.sorted?
puts [1,2].bubblesort!.sorted?
puts [2,1].bubblesort!.sorted?
puts [1, 3, 4].bubblesort!.sorted?
puts [1, 5, 2].bubblesort!.sorted?
puts [1, 10, 9, 2, 5, 6, 6].bubblesort!.sorted?