mrdavid - Algorithm repository
==============================

This repository contains implementations of interesting algorithms in
different languages. 

The goal of this project is not simply to get to know as many algorithms as
possible.

Rather, it is to learn how to think about programming before writing the first
line of code.

Probably like many people who've taught themselves how to write code, I'm used
to using a trial-and-error approach to programming, even professionally. 
I develop ideas by implementing a rough sketch first and then
try out edge cases and run the program to see what, if anything, went wrong.

Ultimately, I would like to get away from this approach and think about what
I'm going to do in more detail beforehand.

(This project was inspired by this article: 
[http://reprog.wordpress.com/2010/04/19/are-you-one-of-the-10-percent/](http://reprog.wordpress.com/2010/04/19/are-you-one-of-the-10-percent/))

### Rules

* If possible, the algorithms must be implemented just by reading a description 
  without consulting a reference implementation.
* The implementation must be carried out by myself and only myself. 
* When implementing the algorithm, trial and error must not be used. It is 
  desireable to sketch out the steps for the implementation on paper first and
  then try to write a correct implementation in one go.
* If after a first implementation, the algorithm was not correctly implemented,
  analysis of the problem should be done on paper first.
* All implementations should come with test cases that must be prepared before
  the program is run for the first time.

Furthermore:

* Any material or discussions used for the implementation must be referenced.
* A commit to the repository must be made *before* writing the first line of
  code and one *after* having written the implementation but *before* running
  the program for the first time.
* Any fixes to an implementation must be commited *before* trying them out
  for the first time.

If for any algorithm the only material available is a reference implementation,
that implementation may be used. Any questions about language syntax must
be clarified before writing the code.

### Index of implemented algorithms

<Algorithm name> (list of programming languages): <directory of the implementation>

##### Sorting

* Bubble sort (ruby):
  _bubblesort/_

### Possibly interesting algorithms not yet implemented

* Quicksort
* Knuth string comparison algorithm
*  Bailye-Borwein-Plouffe formula for calculating arbitrary binary digits of pi.
   http://en.wikipedia.org/wiki/Bailey%E2%80%93Borwein%E2%80%93Plouffe_formula
* Calculate pi using a Monte Carlo algorithm
  http://math.fullerton.edu/mathews/n2003/montecarlopimod.html
* Pop in O(1)
  http://analgorithmaday.blogspot.jp/2011/06/implement-push-pop-min-in-o1.html
* Fastest matrix multiplication in O(n^2.xx)
  http://en.wikipedia.org/wiki/Coppersmith%E2%80%93Winograd_algorithm
* 3d rolling ball algorithm
  http://n-e-r-v-o-u-s.com/blog/?p=3093